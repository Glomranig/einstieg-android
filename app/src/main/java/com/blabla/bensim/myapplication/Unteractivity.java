package com.blabla.bensim.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class Unteractivity extends ActionBarActivity {

    private Joke joke;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unteractivity);
        textView = (TextView) findViewById(R.id.tv_unter);

        //Hole den Intent ab und speichere alle seine Anhänge in ein Bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle.getString("Inhalt").isEmpty()) {
            getRandomJoke();
        } else {
            getJoke(Integer.parseInt(bundle.getString("Inhalt")));
        }
    }

    private void getRandomJoke(){
        String random = "http://api.icndb.com/jokes/random";
        getJson(random);
    }

    private void getJoke(int id){
        String specific = "http://api.icndb.com/jokes/" + id;
        getJson(specific);
    }

    private void getJson(String url){
        //Initialisiere Gson, um später Json in Java Objekte umzuwandeln
        final Gson gson = new Gson();
        //Initialisiere die RequestQueue, welche sämtliche Request Objekte annimmt und abarbeitet
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //Requestobjekt mit URL, Erfolg/Misserfolg Listener initialsieren
        JsonObjectRequest objectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            //Was soll passieren, wenn Request erfolgreich war
            @Override
            public void onResponse(JSONObject response) {

                try {
                    //speichere das Json-Objekt direkt in ein Java Objekt. Benötigt JsonObjekt String und die Java.class
                    joke = gson.fromJson(response.getJSONObject("value").toString(), Joke.class);
                    textView.setText(joke.getJoke());
                } catch (JSONException e) {
                    textView.setText("Netzwerkfehler!");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            //Was passiert, wenn Request nicht erfolgreich war
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );
        //Request Objekt der Queue hinzufügen, damit Anfrage ausgeführt wird
        requestQueue.add(objectRequest);
    }
}
