package com.blabla.bensim.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListenActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{

    private String[] array = {"AA", "BB", "CC", "DD", "EE"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listen);
        //Adapter bilden eine Brücke zwischen Listen (ListView) und den Daten (hier Array)
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        ListView listView = (ListView) findViewById(R.id.listView);
        //Listview mit adapter verbinden
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
    }

    //OnItemClickListener implementieren, um die gedrückte Position und deren Infos zu erhalten
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(
                this,
                position+1 + ". Button wurde gedrückt mit Inhalt: " + array[position],
                Toast.LENGTH_SHORT
        ).show();
    }
}
