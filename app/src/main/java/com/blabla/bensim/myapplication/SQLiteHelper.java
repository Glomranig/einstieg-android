package com.blabla.bensim.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by simonisb on 22.03.2016.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    //Datenbank Name + Versionsnummer
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "names.db";

    //Tabellen Name mit Spalten
    public static final String TABLE_NAMES = "names";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";

    //SQL-Statements
    private static final String SQL_CREATE_NAME =   "CREATE TABLE " + TABLE_NAMES +
                                                    " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                    COLUMN_NAME + " TEXT);";

    private static final String SQL_DROP_NAME =     "DROP TABLE IF EXISTS " + TABLE_NAMES + ";";

    //Verberge den Datenbanknamen und Version nach außen.
    public SQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //Wird ausgeführt, wenn noch keine DB vorhanden ist
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_NAME);
    }

    //Wird ausgeführt, wenn eine neuere Datenbankversion vorhanden ist.
    //Meist sehr komplex, um Daten in das neue Datenbankschema zu übertragen.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_NAME);
        onCreate(db);
    }

    // Hier könnten weitere Methoden stehen, um den Zugriff auf die Datenbank zu erleichtern
    // Bspw. Insert Methoden zu bestimmten Tabellen, welche als Übergabeparameter die Daten erhalten

}
