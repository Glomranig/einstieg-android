package com.blabla.bensim.myapplication;

/**
 * Created by simonisb on 18.03.2016.
 */
public class Joke {

    //Java Objekte, welche aus JsonObjekte generiert werden, benötigen keinen Konstruktor.
    //Konstruktor kann aber hinzugefügt werden, wenn Objekt auch über andere Wege initialisiert werden soll/kann

    private int id;
    private String joke;
    private String[] categories;

    public String getJoke(){
        return joke;
    }

}
