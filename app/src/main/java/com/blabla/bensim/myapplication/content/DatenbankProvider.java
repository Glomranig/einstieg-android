package com.blabla.bensim.myapplication.content;

/*
 * http://www.techotopia.com/index.php/An_Android_Content_Provider_Tutorial
 *
 */

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.blabla.bensim.myapplication.BuildConfig;
import com.blabla.bensim.myapplication.SQLiteHelper;

//ContentProvider wird mit Start der App Initialisiert und benötigt daher keiner Initialisierung durch irgendwelche Activities

public class DatenbankProvider extends ContentProvider {

    private SQLiteHelper sHelper;

    //AUTHORITY = vollständiger Pfad zum contentprovider
    //CONTENT_URI = Verweis auf die Datenbanktabelle "names" und parsing als Uri Objekt
    private static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".content.DatenbankProvider";
    public static final Uri CONTENT_URI_FOR_NAMES = Uri.parse("content://" + AUTHORITY + "/" + SQLiteHelper.TABLE_NAMES);

    //Random Werte, um anfragen nach bestimmten werten zu vereinfachen
    public static final int NAMES = 1;
    public static final int NAMES_ID = 2;


    private static final UriMatcher sUrimatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUrimatcher.addURI(AUTHORITY, SQLiteHelper.TABLE_NAMES, NAMES);
        // "/#" wird hier hinzugefügt, um auf eine zahl hinzuweisen
        sUrimatcher.addURI(AUTHORITY, SQLiteHelper.TABLE_NAMES + "/#", NAMES_ID);
    }

    //Datenbank initialisieren
    @Override
    public boolean onCreate() {
        sHelper = new SQLiteHelper(getContext());
        return (sHelper != null);
    }

    //Liefert einen Cursor mit der übergebenen Select Auswahl aus dem Übergabeparameter
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //Vereinfachte Erstellung von SQLite anfragen
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        //Datenbank initialisieren, um daraus zu lesen
        SQLiteDatabase db = sHelper.getReadableDatabase();
        //Anhand der Uri, können verschiedene Aufrufe angesteuert werden
        switch (sUrimatcher.match(uri)) {
            case NAMES:
                //Querybuilder auf die Tabelle Names setzen
                queryBuilder.setTables(SQLiteHelper.TABLE_NAMES);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        Cursor cursor = null;
        try {
            //Cursor auf Tabelle names der zuvor angegebenen Datenbank und der übergebenen Select Parameter setzen
            cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        } catch (Exception e){
            Log.e("Content Provider", "Fehler bei Abfrage im CP " + e);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    // Werte in unsere Datenbank schreiben
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        //Initialisierung der Switch Case Variabel mit Überprüfung, ob die abgefragte Uri vorhanden ist
        int uriType = sUrimatcher.match(uri);

        SQLiteDatabase db = sHelper.getWritableDatabase();

        long id = 0;
        switch (uriType) {
            case NAMES:
                //ähnlich wie bei query, nur mit den zusätzlichen values und direktem Insert in die datenbank
                id = db.insert(SQLiteHelper.TABLE_NAMES, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unkown Uri : " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(SQLiteHelper.TABLE_NAMES + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
