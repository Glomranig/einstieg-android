package com.blabla.bensim.myapplication;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.blabla.bensim.myapplication.content.DatenbankProvider;

import java.util.ArrayList;

public class Kontaktliste extends ActionBarActivity {

    private EditText editText;
    private ListView listView;
    private String text;
    private Context context;
    private ContentResolver contentResolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontaktliste);
        editText = (EditText) findViewById(R.id.et_db);
        listView = (ListView) findViewById(R.id.lv_db);
        context = this;
        //ContentResolver arbeiten mit dem Contentprovider zusammen.
        contentResolver = this.getContentResolver();
        kontakteLaden();
    }

    private void kontakteLaden() {
        ArrayList<String> arrayList = new ArrayList();
        //resolver erhält vom Datenbankprovider einen Cursor durch die URI auf die komplette Tabelle names
        Cursor cursor = contentResolver.query(DatenbankProvider.CONTENT_URI_FOR_NAMES, null, null, null, null, null);

        //Durchiterieren, um Inhalt in ArrayList zu speichern und mit Listview verbinden zu können
        cursor.moveToFirst();

        do {
            arrayList.add(cursor.getString(cursor.getColumnIndexOrThrow(SQLiteHelper.COLUMN_NAME)));
        } while (cursor.moveToNext());

        //siehe ListenActivity
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);

        listView.setAdapter(arrayAdapter);

    }


    public void onClick(View view){
        text = editText.getText().toString();
        if (!text.isEmpty()) {
            //speichere den Inhalt des Edittext in ein ContentValue, welcher als Key den Spaltennamen der Tabelle name erhält
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteHelper.COLUMN_NAME, text);
            //Nutze Insert Methode des ContentProviders
            contentResolver.insert(DatenbankProvider.CONTENT_URI_FOR_NAMES, contentValues);
            kontakteLaden();
            text = null;
            editText.setText("");
        } else {
            Toast.makeText(this, "Kerle, du musst noch einen Text einfügen", Toast.LENGTH_SHORT).show();
        }
    }
}
