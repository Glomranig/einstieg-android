package com.blabla.bensim.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    //unterAufrufen ist die im layout xml hinterlegte onClick methode, welche aufgerufen wird, sollte auf einen button gedrückt werden

    public void unterAufrufen(View view){
        EditText et = (EditText) findViewById(R.id.et_main);

        //Übergabe View, ist der Ursprung, von wo aus die Methode aufgerufen wurde.
        switch (view.getId()) {

            case R.id.btn_main:
                //Intent = Absichtserklärung. von wo nach wo soll eine Activity geöffnet, bzw. zurückgekehrt werden
                Intent intent = new Intent(this, Unteractivity.class);
                //Intent können Extra informationen enthalten. mit putExtra können diese an einen Intent angehängt werden
                intent.putExtra("Inhalt",
                        et.getText().toString());

                //Startet eine neue Activity mit Hilfe eines Intents
                startActivity(intent);
                break;

            case R.id.btn_list:
                startActivity(new Intent(this, ListenActivity.class));
                break;

            case R.id.btn_website:

                //impliziter Intent. Mit Intent.ACTION_VIEW kann die Art der Aktion festgelegt werden und mit dem Inhalt
                // von Uri.parse() was gemacht werden sollte. Implizite Intents lassen dem User die Wahl, womit die Aktion
                // ausgeführt werden soll
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.reutlingen-university.de/")));
                break;

            case R.id.btn_contact:
                startActivity(new Intent(this, Kontaktliste.class));
                break;
            case R.id.button:
                Toast.makeText(this, "dsvoioinvdoinvoie", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(this, GruppeZwei.class);
                intent1.putExtra("bla", et.getText().toString());
                startActivity(intent1);
                break;
            case R.id.btn_gps:
                startActivity(new Intent(this, Sensoren.class));
                break;
            case R.id.btn_cam:
                startActivity(new Intent(this, Kamera.class));
                break;
        }
    }


}
