package com.blabla.bensim.myapplication;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Sensoren extends ActionBarActivity implements LocationListener{

    private TextView latitute;
    private TextView longitute;
    private LocationManager locationManager;
    private String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensoren);

        latitute = (TextView) findViewById(R.id.tv_unknown1);
        longitute = (TextView) findViewById(R.id.tv_unknown2);

        //Initialisiere den LocationManager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        //Setze den GPS Provider und speichere die aktuelle Position
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        // Initialize the location fields
        if (location != null) {
            Toast.makeText(this, "Provider " + provider + " has been selected.", Toast.LENGTH_SHORT).show();
            onLocationChanged(location);
        } else {
            latitute.setText("Location not available");
            longitute.setText("Location not available");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Initialisiere die Updatefrequenz für den Location Manager
        locationManager.requestLocationUpdates(provider, 10, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Updatefrequenz entfernen
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        //Setze die TextViews auf die Werte des LocationManagers
        latitute.setText(String.valueOf(location.getLatitude()));
        longitute.setText(String.valueOf(location.getLongitude()));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        //Hier können funktionen eingeführt werden, was passieren soll, wenn GPS an oder aus gemacht wird
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(this, "Enabled new provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_SHORT).show();
    }
}
