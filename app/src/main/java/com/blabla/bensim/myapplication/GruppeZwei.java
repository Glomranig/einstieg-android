package com.blabla.bensim.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GruppeZwei extends ActionBarActivity {

    private TextView textView;
    private ListView listView;
    private String[] array = {"AAAA", "BBBBB", "CCCCCC"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gruppe_zwei);
        textView = (TextView) findViewById(R.id.tv_flasche);
        listView = (ListView) findViewById(R.id.lv_bla);

        Bundle bundle = getIntent().getExtras();

        if (!bundle.getString("bla").isEmpty()){
            textView.setText(bundle.getString("bla"));
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>
                        (       this,
                                android.R.layout.simple_list_item_1,
                                array
                        );

        listView.setAdapter(adapter);
    }
}
